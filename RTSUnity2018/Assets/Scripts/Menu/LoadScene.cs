﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField]
    private string _Scenename;

    public void loadScene()
    {
        SceneManager.LoadScene(_Scenename);
    }
    public void endGame() // ends game, centralized in one Script to make administrating easier, could be usid in both scenes
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
