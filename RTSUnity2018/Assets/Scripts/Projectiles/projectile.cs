﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    public float _speed;
    public float _damage;
    public string _tag;
    public float _lifetime;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * _speed;
        Destroy(this.gameObject, _lifetime);
    }
    void Update()
    {
        
    }
   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == _tag)
        {
            //EnemyHealth health = other.gameObject.GetComponent<EnemyHealth>();
            //health._health -= _damage;
            if (other.gameObject.GetComponent<EnemyHealth>() != null)
            {
                other.gameObject.GetComponent<EnemyHealth>()._health -= _damage;
                Destroy(this.gameObject);
            }
            else if (other.gameObject.GetComponent<AllyHealth>() != null) // unelegant, but to be optimized later
            {
                other.gameObject.GetComponent<AllyHealth>()._health -= _damage;
                Destroy(this.gameObject);
            }
            else
            {
                return;
            }

        }
        else
        {
            //Destroy(this.gameObject);
        }
    }
}
