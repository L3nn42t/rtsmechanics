﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bullet", menuName = "Projectiles/New Bullet")]
public class Bullet : ScriptableObject
{
    public float _bulletSpeed; // for now just used in Script EnemyShooting
    public float _damage;
    public float _lifetime;

    public string _targetTag;
}
