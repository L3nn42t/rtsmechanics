﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSWithCollider : MonoBehaviour
{
    public List<GameObject> _selectedObjects = new List<GameObject>();

    List<GameObject> _oldpositions = new List<GameObject>();

    private bool _groupSelectActive;

    [SerializeField]
    private GameObject ColliderObject;//prefab
    [SerializeField]
    private GameObject IndicatorObject;
    [SerializeField]
    private GameObject EmptyObject;


    public Vector3 tempVector; // used 
    public void Update() //One should really use a message system, but for just this task its to complicated to implement one, lesson for future Games
    {
        MoveWithMouse();
        if (Input.GetMouseButtonDown(1))
        {
            //Move here
            MoveToPosition();
        }

        if (Input.GetMouseButtonDown(0) && _groupSelectActive == false)
        {
            //Deselect();
            _groupSelectActive = true;
            tempVector = transform.position;
            //GroupSelect
            Debug.Log(_groupSelectActive.ToString());

        }
        if (Input.GetMouseButtonUp(0) && _groupSelectActive == true)
        {

            GroupSelect(tempVector, transform.position);
            _groupSelectActive = false;
        }
    }

    void MoveToPosition()
    {
        Transform mposition = transform;

        foreach (GameObject @object in _selectedObjects)
        {
            GameObject wpoint = Instantiate(EmptyObject, mposition);
            _oldpositions.Add(wpoint);
            @object.GetComponent<EnemyMovement>().target = wpoint.transform;
        }

    }
    void FindAlternativePosition()
    {

    }

    public void Deselect() // wie genau, dem boden einen Tag geben?
    {

        foreach (GameObject @object in _selectedObjects)
        {
            @object.GetComponent<Selectable>().DeselectThis();
            //_selectedObjects.Remove(@object);
        }
        _selectedObjects.Clear();
    }
    float maxColliderDistance() //To be used to calculate the optimal distance between two gameobjects;
    {
        float widht = 0f;
        foreach (GameObject gameObject in _selectedObjects)
        {
            if (widht < gameObject.GetComponent<CapsuleCollider>().radius * 2)
            {
                widht = gameObject.GetComponent<CapsuleCollider>().radius * 2;
            }

        }
        return widht;
    }

    void GroupSelect(Vector3 start, Vector3 end)
    {

        //Vector3 east = new Vector3(start.x, 0, end.z);
        //Vector3 west = new Vector3(end.x, 0, start.z);
        float h = start.x + end.x;
        float w = start.z + end.z;
        Transform ttransform = transform;
        ttransform.position = new Vector3(h, 0, w);

        SpawnCollider(ttransform, h, w);
    }
    public void MoveWithMouse()
    {
        Vector3 mouse = Input.mousePosition;
        Ray castPoint = Camera.main.ScreenPointToRay(mouse);
        RaycastHit hit;
        if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
        {
            transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        }


    }
    //One could probably do something more performant, but we shoud do our own thing, the use of a collider could be usefull to select enemy units, I have never seen that
    public void SpawnCollider(Transform targetransform, float height, float width)
    {
        if (height < 0)
        {
            height *= -1;
        }
        if (width < 0)
        {
            width *= -1;
        }
        GameObject @object = Instantiate(ColliderObject, targetransform);
        @object.transform.parent = null;
        //@object.GetComponent<SelectCollider>().tselection = this;
        @object.GetComponent<BoxCollider>().size = new Vector3(height, 2, width);
        @object.GetComponentInChildren<Transform>().localScale = new Vector3(height, 2, width);
        //Destroy(@object, 0.2f);
    }

    void SpawnEmptyPosition(Transform transform)
    {

    }
}
