﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GUISelector : MonoBehaviour
{
    [SerializeField]
    private RTSselection _selection;
    [SerializeField]
    private InfoDisplayer _infoDisplayer;

    bool _isSelecting = false;
    Vector3 _mousePosition;

    void Update()
    {
        // If we press the left mouse button, begin selection and remember the location of the mouse
        if (Input.GetMouseButtonDown(0))
        {
            _isSelecting = true;
            _mousePosition = Input.mousePosition;

            foreach (var selectableObject in FindObjectsOfType<Selectable>())
            {
                if (selectableObject._isSelected == true)
                {
                    _selection.Deselect();
                    //selectableObject.DeselectThis();
                }
            }
        }
        // If we let go of the left mouse button, end selection
        if (Input.GetMouseButtonUp(0))
        {
            var selectedObjects = new List<Selectable>();
            foreach (var selectableObject in FindObjectsOfType<Selectable>())
            {
                if (IsWithinSelectionBounds(selectableObject.gameObject))
                {
                    
                    
                        selectedObjects.Add(selectableObject);
                    
                    
                }
            }


            _infoDisplayer.DisplayInfo();
            _isSelecting = false;
        }

        //Actually selecting units
        if (_isSelecting)
        {
            foreach (var selectableObject in FindObjectsOfType<Selectable>())
            {
                if (IsWithinSelectionBounds(selectableObject.gameObject))
                {
                    if (selectableObject._isSelected == false)
                    {
                        if (selectableObject.gameObject.GetComponent<EnemyHealth>()._active == true)
                        {
                            selectableObject.SelectThis();
                        }
                            
                    }
                }
                else
                {
                    if (selectableObject._isSelected != false)
                    {
                        
                        selectableObject._isSelected = false;
                    }
                }
            }
        }
    }

    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!_isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = GUIUtility.GetViewportBounds(camera, _mousePosition, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    void OnGUI()
    {
        if (_isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = GUIUtility.GetScreenRect(_mousePosition, Input.mousePosition);
            GUIUtility.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            GUIUtility.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }
}
