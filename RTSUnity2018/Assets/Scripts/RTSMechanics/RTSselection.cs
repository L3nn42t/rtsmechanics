﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSselection : MonoBehaviour
{

    public List<GameObject> _selectedObjects = new List<GameObject>();

    List<GameObject> _oldpositions = new List<GameObject>();

    private bool _groupSelectActive;

    [SerializeField]
    private GameObject ColliderObject;//prefab
    [SerializeField]
    private GameObject IndicatorObject;
    [SerializeField]
    private GameObject EmptyObject;
    [SerializeField]
    private GameObject _marker;
    public Vector3 tempVector; // used 
    public void Update() //One should really use a message system, but for just this task its to complicated to implement one, lesson for future Games
    {
        MoveWithMouse();
        if (Input.GetMouseButtonDown(1))
        {
            //Move here
            if(_selectedObjects.Count >= 1)
            {
                MoveToPosition();
            }
        }

        
        
       
    }

    void MoveToPosition()
    {
        foreach(GameObject oobject in _oldpositions)
        {
            //Destroy(oobject, 1f); // used to avoid to many objects
            //_oldpositions.Clear();
        }



        
        int iterationnumber = _selectedObjects.Count;
        int iteration = 0;
        do
        {
            GameObject @object = _selectedObjects[iteration];

            Transform mposition = transform;
            FindAlternativePosition(mposition, iteration+1);
            GameObject wpoint = Instantiate(EmptyObject, mposition);
            wpoint.transform.parent = null;
            _oldpositions.Add(wpoint);

            @object.GetComponent<EnemyMovement>()._reachedEndOfPath = false;
            @object.GetComponent<EnemyMovement>()._ismoving = true;
            
            @object.GetComponent<EnemyMovement>().target = wpoint.transform;
            @object.GetComponent<EnemyMovement>().UpdatePath();


            iteration++;
        }
        while (iteration < iterationnumber);

        GameObject mrk = Instantiate(_marker, transform);
        mrk.transform.parent = null;
    }


   


    void FindAlternativePosition(Transform ntransform, int number)
    {
        float mindistance = maxColliderDistance();
        
        switch (number)
        {
            case 0: // anying mistake, but fixing is anoying
                Debug.LogError("Case0");
                break;
            case 1:
                ntransform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                break;
                
            case 2:
                ntransform.position = new Vector3(ntransform.position.x + mindistance, ntransform.position.y, ntransform.position.z);
                break;
            case 3:
                ntransform.position = new Vector3(ntransform.position.x - mindistance, ntransform.position.y, ntransform.position.z);
                break;
            case 4:
                ntransform.position = new Vector3(ntransform.position.x, ntransform.position.y, ntransform.position.z + mindistance);
                break;
            case 5:
                ntransform.position = new Vector3(ntransform.position.x, ntransform.position.y, ntransform.position.z - mindistance);
                break;
            case 6:
                ntransform.position = new Vector3(ntransform.position.x + mindistance, ntransform.position.y, ntransform.position.z + mindistance);
                break;
            case 7:
                ntransform.position = new Vector3(ntransform.position.x + mindistance, ntransform.position.y, ntransform.position.z - mindistance);
                break;
            case 8:
                ntransform.position = new Vector3(ntransform.position.x - mindistance, ntransform.position.y, ntransform.position.z + mindistance);
                break;
            case 9:
                ntransform.position = new Vector3(ntransform.position.x - mindistance, ntransform.position.y, ntransform.position.z - mindistance);
                break;
        }
            
            
    }

    public void Deselect() // wie genau, dem boden einen Tag geben?
    {
        
        foreach(GameObject @object in _selectedObjects)
        {
            @object.GetComponent<Selectable>().DeselectThis();
            //_selectedObjects.Remove(@object);
        }
        _selectedObjects.Clear();
    }
    float maxColliderDistance() //To be used to calculate the optimal distance between two gameobjects;
    {
        float widht = 0f;
        foreach(GameObject gameObject in _selectedObjects)
        {
            if(widht < gameObject.GetComponent<CapsuleCollider>().radius*2)
            {
                widht = gameObject.GetComponent<CapsuleCollider>().radius*2;
            }
            
        }
        return widht;
    }

    
    public void MoveWithMouse()
    {
        Vector3 mouse = Input.mousePosition;
        Ray castPoint = Camera.main.ScreenPointToRay(mouse);
        RaycastHit hit;
        if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
        {
            transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        }


    }
    //One could probably do something more performant, but we shoud do our own thing, the use of a collider could be usefull to select enemy units, I have never seen that
    

    void SpawnEmptyPosition(Transform transform)
    {

    }
}
