﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetObjectActive : MonoBehaviour
{
    [SerializeField]
    private GameObject @object;
    [SerializeField]
    public bool _isactive = false;
    public void Active() //to be used with a button
    {
        if (_isactive == true)
        {
            @object.SetActive(false);
            _isactive = false;
        }
        if (_isactive == false)
        {
            @object.SetActive(true);
            _isactive = true;
        }


    }
}
