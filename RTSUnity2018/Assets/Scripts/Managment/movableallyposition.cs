﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movableallyposition : MonoBehaviour
{
    [SerializeField]
    private GameObject allyPrefab;
    [SerializeField]
    private Color _hcolor;
    private Color _uColor;

    private bool _buildingactive;
    private Color _scolor;
    private Renderer _renderer;
    [SerializeField]
    private Manager manager;
    public void Start()
    {
        _renderer = GetComponent<Renderer>();
        _scolor = _renderer.material.color;
        //this.gameObject.SetActive(false);
        //Active();
    }

    public void Update()
    {
        MoveWithMouse();
        
        
      if (Input.GetMouseButtonDown(0))
      {
            //NewInput();
            StartCoroutine(NInput());
      }
      if (Input.GetMouseButtonDown(1))
      {
            this.gameObject.SetActive(false);
      }
        
    }
    
    //_renderer.material.color = _hcolor;
         
    public void MoveWithMouse()
    {
        Vector3 mouse = Input.mousePosition;
        Ray castPoint = Camera.main.ScreenPointToRay(mouse);
        RaycastHit hit;
        if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
        {
            transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        }

        
    }
   public void NewInput()
    {
        
        
    }
    IEnumerator NInput()
    {
        if (manager._buildpoints >= 1)
        {
            _renderer.material.color = _hcolor;
            Transform tposition = transform;
            AllySpawner allySpawner = FindObjectOfType<AllySpawner>();
            allySpawner.SpawnAlly(tposition, allyPrefab);
            manager._buildpoints -= 1;
            _renderer.material.color = _scolor;

            this.gameObject.SetActive(false);
            yield return new WaitForSeconds(1f);
            yield break;
        }
    }



    public void OnMouseExit()
    {
        _renderer.material.color = _scolor;
    }
}
