﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoal : MonoBehaviour
{
    [SerializeField]
    private Manager _manager;

    // Start is called before the first frame update
    void Start()
    {
        _manager = FindObjectOfType<Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            _manager._playerlives -= 1;
            //_manager.activeEnemys -= 1;
            // maybe do a death animation here
            Destroy(other.gameObject);
        }
    }
}
