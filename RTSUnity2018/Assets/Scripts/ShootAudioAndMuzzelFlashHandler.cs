﻿using System;
using System.Collections;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(AudioSource))]
	public class ShootAudioAndMuzzelFlashHandler : MonoBehaviour
	{
		[Tooltip("The audio event that should be played when the animation event is happening")] [SerializeField]
		private ScriptableAudioEvent _shootAudioEvent;
		
		[SerializeField]
		private GameObject _muzzleFlash;
		private AudioSource _audioSource;

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
			_muzzleFlash.SetActive(false);
		}

		// Called as an animation event
		private void DoShootSound()
		{
			_shootAudioEvent.Play(_audioSource);

			StartCoroutine("MuzzleFlash");
		}

		private IEnumerator MuzzleFlash()
		{
			_muzzleFlash.SetActive(true);
			yield return new WaitForSeconds(0.2f);
			if (_muzzleFlash != null)
			{
				_muzzleFlash.SetActive(false);
			}
			
		}
	}
}