﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject placeholder;
    public void SpawnAlly(Transform allytransform, GameObject allyPrefab)
    {
        GameObject gameObject = Instantiate(placeholder, allytransform.position, allytransform.rotation);
        GameObject newally = Instantiate(allyPrefab, transform.position, transform.rotation);
        newally.GetComponent<EnemyMovement>().target = gameObject.transform;
        return;
        // Give Ally his position here
    }
}
