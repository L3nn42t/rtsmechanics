﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyHealth : MonoBehaviour
{
    // Is the EnemyHealthscript, without references to  the manager, in future a message could be sent;
    public float _health;

    public bool _active;
    [SerializeField]
    private float _deathtime = 10f;

    private AnimationHandler _handler;
    private void Start()
    {
        _handler = GetComponent<AnimationHandler>();
        _active = true;
        
    }
    private void Update()
    {
        if (_health <= 0f)
        {
            Death();
        }
    }

    public void Death()
    {

        _active = false;
        EnemyMovement movement = GetComponent<EnemyMovement>();
        movement._ismoving = false;
        //Do a death Animation here
        Debug.Log("One of your soldiers of type" + name + "is dead");
        _handler.Die();
        Destroy(this.gameObject, _deathtime);
    }
}
