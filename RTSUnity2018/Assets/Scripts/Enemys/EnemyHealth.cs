﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    
    public float _health;

    [SerializeField]
    private int _pointreward = 1;
    private Manager _manager;

    public bool _active;
    [SerializeField]
    private float _deathtime = 10f;

    private AnimationHandler _handler;
    private void Start()
    {
        _handler = GetComponent<AnimationHandler>();
        _active = true;
        _manager = FindObjectOfType<Manager>();
    }
    private void Update()
    {
        if (_health <= 0f && _active)
        {
            Death();
        }
    }

    public void Death()
    {
        
        _active = false;
        
        EnemyMovement movement = GetComponent<EnemyMovement>();
        movement._ismoving = false;
        //_manager.activeEnemys -= 1; // temp exeption
        //Do a death Animation here
        _manager._buildpoints += _pointreward;
        _handler.Die();
        Destroy(this.gameObject, _deathtime);
    }
}
