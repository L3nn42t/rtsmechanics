﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    //[SerializeField]
    //private GameObject[] Enemys;

    [SerializeField]
    private Dictionary<GameObject, float> _enemyDictionary = new Dictionary<GameObject, float>();
    [SerializeField]
    private GameObject[] _enemys;
    public bool _waveActive = false;
    
    public float spawnamount;

    [SerializeField]
    private Transform[] Spawnlocations;

    private int _previousPoint;
    public void Start()
    {
        DebugContent();
        if (_enemys == null)
        {
            _enemys = GameObject.FindGameObjectsWithTag("enemy");
        }
        foreach (GameObject enemy in _enemys)
        {
            // _enemyDictionary.Add(_enemys, );
        }
        
    }

    public void Update()
    {
        if (_waveActive == true)
        {
            Spawning();
            
        }

    }

    public void Spawning()
    {
        foreach (KeyValuePair<GameObject, float> enemys in _enemyDictionary)
        {
            StartCoroutine(EnemySpawner(enemys.Key, enemys.Value));
        }
    }
    public string toString()
    {
        string content = $"Spawner {name} contains:\r\n";
        foreach (KeyValuePair<GameObject,float> edictionary in _enemyDictionary)
        {
            content += $"[{edictionary.Key.name} - {edictionary.Value}]\r\n";

            
        }
        return content;
    }
    public void DebugContent()
    {
        Debug.Log(toString());
    }

    IEnumerator EnemySpawner(GameObject enemy, float cost)
    {
        int index = Random.Range(0, Spawnlocations.Length);
        Transform sPoint = Spawnlocations[index];
        if (spawnamount <= cost)
        {
            if (index != _previousPoint)
            {
                yield break;
            }
            else
            {
                Instantiate(enemy, sPoint);
                index = _previousPoint;
                spawnamount -= cost;
                yield return cost;
            }
            
        }
        else
        {
            _waveActive = false;
        }
            
    }
}
