﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        MoveCamera();
        //Quaternion quaternion = new Quaternion()

    }

    void MoveCamera()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        float mouseinput = Input.GetAxis("Mouse ScrollWheel") * -100f;
        Vector3 direction = new Vector3(horizontalInput, mouseinput, verticalInput).normalized;
        transform.position += direction;
    }
}
