﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectInfoDisplay : MonoBehaviour
{
    public SelectInfo _info;


    [SerializeField]
    private Text _nametext;
    [SerializeField]
    private Text healthText;

    [SerializeField]
    private Image _healthimage;
    [SerializeField]
    private Image _damageimage;
    public void Start()
    {
        _healthimage.enabled = true;
        _damageimage.enabled = false;
        _nametext.text = _info._displayName;
    }

    public void Update()
    {
        healthText.text = _info._displayHealth+" / " +_info._maxhealth.ToString();

        if(_info._displayHealth <= _info._maxhealth / 3)
        {
            _healthimage.enabled = false;
            _damageimage.enabled = true;
        }
    }
}
