﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    [SerializeField]
    private GameObject selectImage;

    public bool _isSelected = false;

    public RTSselection sselection;

    public void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && Input.GetKeyDown(KeyCode.LeftShift) && _isSelected == false)
        {
            SelectThis();
        }
        else if (Input.GetMouseButtonDown(0) && _isSelected == false)
        {
            //Insert selection here
            //sselection._selectedObjects.Add(this.gameObject);
            sselection.Deselect();
            SelectThis();
        }
        else if (Input.GetMouseButtonDown(0) && _isSelected == true)
        {
            GameObject[] others = GameObject.FindGameObjectsWithTag(this.gameObject.tag);

            foreach(GameObject @object in others)
            {
                Selectable oselectable = @object.GetComponent<Selectable>();
                if(oselectable._isSelected == false)
                {
                    oselectable.SelectThis();
                }
                
            }
        }
        //implement double click here
    }
    public void SelectThis()
    {
        _isSelected = true;
        sselection._selectedObjects.Add(this.gameObject);
        selectImage.SetActive(true);
    }
    public void DeselectThis()
    {
        _isSelected = false;
       
        selectImage.SetActive(false);
    }
    
}
