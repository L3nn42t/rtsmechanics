﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectCollider : MonoBehaviour
{
    public RTSselection tselection;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Selectable>() != null)
        {
            if(other.gameObject.GetComponent<Selectable>()._isSelected == false)
            {
                other.gameObject.GetComponent<Selectable>().SelectThis();
                

            }
        }
    }
}
