﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InfoDisplayer : MonoBehaviour
{
    public RTSselection _rtsselection;

    public float distance;
    public GameObject Infoprefab;
    List<GameObject> _displays = new List<GameObject>();

    public void Update()
    {
        
    }


    public void DisplayInfo()
    {
        Replace();
        for(int i= 0; i < _rtsselection._selectedObjects.Count; i++)
        {
            GameObject selected = _rtsselection._selectedObjects[i];

            SpawnInfo(i, selected.GetComponent<SelectInfo>());
        }
    }
    void Replace()
    {
        foreach(GameObject @object in _displays)
        {
            Destroy(@object);

        }
        _displays.Clear();
    }


    void SpawnInfo(int numbercase, SelectInfo info)
    {
        
        Vector3 pos = transform.position;
        switch (numbercase)
        {
            case 0:
                
                pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                break;
            case 1:
                pos = new Vector3(transform.position.x + distance, transform.position.y, transform.position.z);
                break;
            case 2:
                pos = new Vector3(transform.position.x + distance*2, transform.position.y, transform.position.z);
                break;
            case 3:
                pos = new Vector3(transform.position.x+ distance*3, transform.position.y, transform.position.z);
                break;
            case 4:
                pos = new Vector3(transform.position.x+ distance*4, transform.position.y, transform.position.z);
                break;
            case 5:
                pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                break;
            case 6:
                pos = new Vector3(transform.position.x+distance, transform.position.y, transform.position.z);
                break;
            case 7:
                pos = new Vector3(transform.position.x+distance*2, transform.position.y, transform.position.z);
                break;
            case 8:
                pos = new Vector3(transform.position.x+distance*3, transform.position.y, transform.position.z);
                break;
            case 9:
                pos = new Vector3(transform.position.x+distance*4, transform.position.y, transform.position.z);
                break;
        }

        GameObject newInfo = Instantiate(Infoprefab, transform);
        newInfo.GetComponent<RectTransform>().anchoredPosition = pos;
        _displays.Add(newInfo);
        newInfo.GetComponent<SelectInfoDisplay>()._info = info;
        
    }
}
