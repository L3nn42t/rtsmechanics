﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class EnemyShooting : MonoBehaviour
{
    public GameObject _enemyTarget;

    [SerializeField]
    private float _targetDistance;
    [SerializeField]
    private float _fireDistance;
    [SerializeField]
    private float _losedistance;

    [SerializeField]
    private float _measureTime; //used to determine in which intervall the enemy leads, used both in Method and in Invoke
    [SerializeField]
    private Vector3 _fposition;

    [SerializeField]
    private Bullet _bullet;

    private Vector3 p1;
    private Vector3 p2;

    [SerializeField]
    private float firespeed;
    [SerializeField]
    private GameObject _projectilePrefab;
    [SerializeField]
    private Transform _projectileSpawn; //for different weapon types

    private float _lastfire;

    private AnimationHandler _animationHandler;
    
    private EnemyMovement _movement;
    private EnemyHealth _ehealth;
    private AllyHealth _allyHealth;

    [SerializeField]
    private bool _seeking; // used to determine if a target is needed
    private void Start()
    {
        _animationHandler = GetComponent<AnimationHandler>();
        _movement = GetComponent<EnemyMovement>();
        //_health = GetComponent<EnemyHealth>();
        if (_seeking == true)
        {
            FindTarget();
        }
    }

    void Update()
    {
        if (_enemyTarget == null && _seeking == true)
        {
            FindTarget();
            return;
        }

        if (_enemyTarget != null)
        {
            _targetDistance = (_enemyTarget.transform.position - this.transform.position).sqrMagnitude;
            Debug.DrawLine(this.transform.position, _enemyTarget.transform.position);

            if (_targetDistance <= _fireDistance)
            {
                // Debug.Log("leading was started");
                //InvokeRepeating("Lead", _measureTime + 0.1f, _measureTime + 0.1f);  //General Logic needs to be improved   
                StartCoroutine("leading");
            }
            else
            {
                return;
            }
            Debug.DrawLine(this.transform.position, _fposition);

            if ((Time.time - _lastfire) > (1 / firespeed) && _targetDistance <= _fireDistance)
            {
                if (_enemyTarget.GetComponent<EnemyHealth>() != null)
                {
                    _ehealth = _enemyTarget.GetComponent<EnemyHealth>();
                    if(_ehealth._active == true)
                    {
                        _lastfire = Time.time;
                        StartCoroutine("fire");
                        return;
                    }
                    else
                    {
                        FindTarget();
                    }
                }
                else if(_enemyTarget.GetComponent<AllyHealth>() != null)
                {
                    _allyHealth = _enemyTarget.GetComponent<AllyHealth>();
                    if(_allyHealth._active == true)
                    {
                        _lastfire = Time.time;
                        StartCoroutine("fire");
                        return;
                    }
                    else
                    {
                        FindTarget();
                    }
                }
                else
                {
                    return;
                }

                
            }
            if (_targetDistance <= _losedistance)
            {
                //_enemyTarget = null;
                FindTarget();
            }
        }
    }


    private void FindTarget()
    {
        _targetDistance = Mathf.Infinity;
        GameObject[] alltargets = GameObject.FindGameObjectsWithTag(_bullet._targetTag); //rewrote to save effort, should be used by both

        foreach (GameObject enemytarget in alltargets)
        {
            float distanceToTarget = (enemytarget.transform.position - this.transform.position).sqrMagnitude;
            if(distanceToTarget < _targetDistance)
            {
                _targetDistance = distanceToTarget;
                _enemyTarget = enemytarget;
            }
            else
            {
                return;
            }
        }
    }

    public void Lead() // will probably put this in a different script, maybe an interface
    {

        StartCoroutine("leading");
        //_fposition = _enemyTarget.transform.position +((_enemyTarget-transform.position))
    }
    public IEnumerator fire()
    {
        _animationHandler.Shoot();
        _movement._ismoving = false;
        this.gameObject.transform.LookAt(_fposition);
        yield return new WaitForSeconds(_animationHandler._pantsAnimator.GetCurrentAnimatorStateInfo(0).length * 0.75f);
        this.gameObject.transform.LookAt(_fposition);
        GameObject bullet = Instantiate(_projectilePrefab, _projectileSpawn.position, _projectileSpawn.rotation);
        projectile pscript = bullet.GetComponent<projectile>();
        pscript._damage = _bullet._damage;
        pscript._speed = _bullet._bulletSpeed;
        pscript._tag = _bullet._targetTag;
        pscript._lifetime = _bullet._lifetime;
        _movement._ismoving = true;
    }

    IEnumerator leading()
    {
       if (_enemyTarget != null) // 
        {
            p1 = _enemyTarget.transform.position;
            float d1 = _targetDistance / _bullet._bulletSpeed;
            yield return new WaitForSeconds(_measureTime);
            if (_enemyTarget != null)
            {
                float d2 = _targetDistance / _bullet._bulletSpeed;
                _fposition = _enemyTarget.transform.position + ((_enemyTarget.transform.position - p1) * d2 * (_measureTime + (d2 - d1)));
            }
             //coroutine doesn't work when object gets destroyed, maybe a localised one?
            yield return _fposition;

            
            //yield break;
        }

    }
}
