﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    private static readonly int movement = Animator.StringToHash("Movement");
    private static readonly int Death = Animator.StringToHash("Death");
    private static readonly int ShootTrigger = Animator.StringToHash("ShootTrigger");

    [SerializeField]
    private GameObject _pants;
    [SerializeField]
    private GameObject _soldier;
    
    public Animator _pantsAnimator;
    public Animator _soldierAnimator;
    
    private float _cMovement;
    // Start is called before the first frame update
    void Start()
    {
        _pantsAnimator = _pants.GetComponent<Animator>();   
        _soldierAnimator = _soldier.GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        _pantsAnimator.SetFloat(movement, _cMovement);
    }

    public void Shoot()
    {
        _pantsAnimator.SetTrigger(ShootTrigger);
        _soldierAnimator.SetTrigger(ShootTrigger);
    }

    public void Die()
    {
        _pantsAnimator.SetTrigger(Death);
        _soldierAnimator.SetTrigger(Death);
    }

   public void SetSpeed(float Movement)
    {
        _cMovement = Movement;
    }
}
