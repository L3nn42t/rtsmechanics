﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyMovement : MonoBehaviour
{
    // is supposed to be used to stop the enemy when firing in a later script    
    public bool _ismoving = true;

    [SerializeField] private float _movespeed;
    [SerializeField] private float _nextWaypointDistance = 3f;

    public Transform target;

    private Path path;
    private int _currentWaypoint = 0;
    public bool _reachedEndOfPath = false;

    private Seeker seeker;
    private Rigidbody rb; //shall be used to maybe apply force from Explosions

    private AnimationHandler _handler;


    // Start is called before the first frame update
    void Start()
    {
        _handler = GetComponent<AnimationHandler>();
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody>();

        InvokeRepeating("UpdatePath", 0f, 0.5f);
        seeker.StartPath(rb.position, target.position, OnPathComplete);
    }


    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
        }
    }

    public void UpdatePath()
    {
        if (target != null)
        {
            if (seeker.IsDone() && _reachedEndOfPath == false)
                seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (_ismoving == true && target != null) // used to stop when Enemy should target something
        {
            if (path == null)
                return;

            if (_currentWaypoint >= path.vectorPath.Count)
            {
                _reachedEndOfPath = true;
                _ismoving = false;
                _handler.SetSpeed(0f);
            }
            else
            {
                _reachedEndOfPath = false;

                // Debug.Log(name + "is moving");
                // Vector2 _direction = ((Vector2)path.vectorPath[_currentWaypoint] - rb.position).normalized;
                Vector3 _direction = ((Vector3) path.vectorPath[_currentWaypoint] - rb.position).normalized;
                Vector3 _force = _direction * _movespeed * Time.deltaTime;
                rb.AddForce(_force);
                float _distance = Vector3.Distance(rb.position, path.vectorPath[_currentWaypoint]);
                this.transform.rotation = Quaternion.LookRotation(_direction);
                _handler.SetSpeed(1f);

                //trigger for Walking animation here

                if (_distance < _nextWaypointDistance)
                {
                    _currentWaypoint++;
                }
            }
        }
        
        else if (_ismoving == false)
        {
            _handler.SetSpeed(0f);
        }
    }
}
