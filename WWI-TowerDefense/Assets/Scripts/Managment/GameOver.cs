﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour
{
    [SerializeField]
    private Manager _manager;
    [SerializeField]
    private GameObject _gameoverUI;
  
    // Start is called before the first frame update
    void Start()
    {
        _manager = FindObjectOfType<Manager>();
        _gameoverUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (_manager._playerlives <= 0)
        {
            Debug.Log("Game is ending");
            Gameover();
        }
    }
    void Gameover()
    {
        Time.timeScale = 0.05f;
        _gameoverUI.SetActive(true);
    }

    
}
