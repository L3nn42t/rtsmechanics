﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class allyPosition : MonoBehaviour
{
    [SerializeField]
    private GameObject allyPrefab;
    [SerializeField]
    private Color _hcolor;
    private Color _uColor;

    private bool _allyActive;
    private Color _scolor;
    private Renderer _renderer;


    public void Start()
    {
        _renderer = GetComponent<Renderer>();
        _scolor = _renderer.material.color;
    }
    public void OnMouseOver()
    {     
        if (_allyActive == false)
        {
            _renderer.material.color = _hcolor;
            if (Input.GetMouseButtonDown(0))
            {
                AllySpawner allySpawner = FindObjectOfType<AllySpawner>();
                allySpawner.SpawnAlly(transform, allyPrefab);
                this.gameObject.SetActive(false);
            }
        }
        else if (_allyActive == true)
        {
            return;
        }

    }
    public void OnMouseExit()
    {
        _renderer.material.color = _scolor;
    }
}
