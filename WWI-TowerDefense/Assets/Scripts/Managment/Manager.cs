﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Manager : MonoBehaviour
{
    public int _buildpoints = 4;

    public int _wavenumber;

    public bool _waveActive;
    // 
    public int _playerlives = 10;

    public int activeEnemys;

    public float _betweentime = 5f;

    public bool spawning;
    public void Start()
    {
        _wavenumber = 0;
        StartCoroutine(nwave());
    }

    // UI-Stuff
    [SerializeField]
    private TextMeshProUGUI _livetext;

    [SerializeField]
    private TextMeshProUGUI _wavetext;

    [SerializeField]
    private TextMeshProUGUI _enemycounttext;

    [SerializeField]
    private GameObject _wavetextObject;

    [SerializeField]
    private TextMeshProUGUI _buildText;

    public void Update()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");

        activeEnemys = enemies.Length;

        string enemycountconverter = activeEnemys.ToString();

        _enemycounttext.text = enemycountconverter;
        

        if (_wavenumber >= 1 && activeEnemys <= 0)
        {
            if (_waveActive == true)
            {
                
                StartCoroutine(nwave());
            }
        }

        UItoText();
        
    }

    IEnumerator nwave()
    {
        //nt debugnumber = _wavenumber += 1;
        _waveActive = false;
        //_wavetext.enabled = true;
        _wavetextObject.SetActive(true);
        int debugnumber = _wavenumber + 1;
        string wavemessage = "in " + _betweentime.ToString() + " the next wave number " + debugnumber.ToString() + " is starting";
        _wavetext.text = wavemessage;
        Debug.Log("in " + _betweentime + " the next wave number " + _wavenumber + " is starting");
        yield return new WaitForSeconds(_betweentime);
        //_buildpoints += 1;
       
        _wavenumber += 1;
        spawning = true;
        _waveActive = true;

        //_wavetext.enabled = false;
        _wavetextObject.SetActive(false);
    }

    private void UItoText()
    {
        _livetext.text = _playerlives.ToString();
        _buildText.text = _buildpoints.ToString();

    }

    


   
}
