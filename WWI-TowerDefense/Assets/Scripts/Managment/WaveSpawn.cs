﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawn : MonoBehaviour
{
    [SerializeField]
    private Transform _enemytarget;


    [SerializeField]
    private Transform SpawnlocationOne;
    [SerializeField]
    private Transform SpawnlocationTwo;
    [SerializeField]
    private Transform SpawnlocationThree;
    private int _previousPoint;

    [SerializeField]
    private GameObject enemyprefab;
    [SerializeField]
    private Manager _manager;
    //private bool waveactive = false;

    private float _spawndelay = 8f;
    private float countdown = 4f;

    
    // Start is called before the first frame update
    void Start()
    {
        _manager = FindObjectOfType<Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_manager.activeEnemys <= 0)
        {
            _manager._waveActive = true;
            StartCoroutine(Spawnenemywave());
            Debug.Log("wave is spawning");
            countdown = _spawndelay;
            return;
        }
        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0, Mathf.Infinity);

    }


    IEnumerator Spawnenemywave()
    {
        int spawnamount = Mathf.FloorToInt(_manager._wavenumber * 2.99f);
        Transform opoint = null;
        for (int i = 0; i < spawnamount; i++)
        {
            Transform npoint = spoint();

            if (npoint != opoint)
            {
                spawnenemy(npoint);
            }
           
            
             opoint = npoint;
            //_manager.activeEnemys += 1;
            yield return new WaitForSeconds(2f);
        }
        _manager.spawning = false;

        
       
        //for (int i = 0; i < _manager._wavenumber; i++)
        // {
        //    int index = Random.Range(0, Spawnlocations.Length);
        //    Transform sPoint = Spawnlocations[index];//
        //
        //    if (index != _previousPoint)
        //    {
        //        yield break;
        //    }
        //    else
        //    {
        //        spawnenemy(sPoint);
        //       index = _previousPoint;
        //       yield return new WaitForSeconds(0.5f);
        //   }
        //}
    }

    Transform spoint()
    {
        int choice = Mathf.FloorToInt(Random.value * 2.99f);
        switch (choice)
        {
            case 0:
                return SpawnlocationOne;
            case 1:
                return SpawnlocationTwo;
            default:
                return SpawnlocationThree;
        }
    }

    void spawnenemy(Transform position)
    {
        GameObject spawned = Instantiate(enemyprefab, position.position, position.rotation);
        spawned.GetComponent<EnemyMovement>().target = _enemytarget;
        //_manager.activeEnemys += 1;
    }
}
