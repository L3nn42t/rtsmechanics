﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{

    public TextMeshProUGUI timerText;
    private float startTime;

    void Start()
    {
        startTime = Time.time;
    }

   
    void Update()
    {
        float t = Time.time - startTime;

        int minutes = (int) t / 60 ;
        int seconds = (int) t - (60 * minutes);
        
        timerText.text = $"{minutes:00}:{seconds:00}";

            // string minutes = ((int)t / 60).ToString();
            // string seconds = (t % 60).ToString("f00");
            //
            // timerText.text = minutes + ":" + seconds ;
    }
}
