﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private Slider _masterslider;

    private float _masterVolume = 100f;

    private void Update()
    {
        _masterVolume = _masterslider.value; 
    }

}
